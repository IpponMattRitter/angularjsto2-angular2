/**
 * Created by mattritter on 4/4/17.
 */
var path = require('path');

module.exports = {
    entry: {
        app: './app/src/main.ts',
    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'app/prod/js/')
    },
    resolve: {
        extensions: ['webpack.js', 'web.js', '.ts', '.js']
    },
    // you will need the ts-loader modules so that webpack knows what to do with typescript files.
    module: {
        rules: [
            { test: /\.ts$/, exclude: /node_modules/, loaders: [
                'ts-loader',
                'angular-router-loader'
            ]}
        ]
    },
};
