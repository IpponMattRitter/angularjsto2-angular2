import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GenericListComponent }      from './genericList/generic-list.component';
import { GenericDetailComponent }  from './genericDetail/generic-detail.component';

const routes: Routes = [
  { path: '', redirectTo: '/list', pathMatch: 'full' },
  { path: 'list',  component: GenericListComponent },
  { path: 'item/:id', component: GenericDetailComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}