import {Component} from '@angular/core';

@Component({
    selector: 'JSv22',
    template: `
    <div class="container-fluid">
	  <div class="row">
	    <div class="col-md-12">
	      <h1>{{title}}</h1>
	    </div>
	  </div>
	</div>
	<router-outlet></router-outlet>
    `,
})

export class AppComponent {
	private title:string = "Angular2 Version";
}