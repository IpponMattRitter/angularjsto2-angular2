import {Component, OnInit} from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import {GenericService} from '../sharedServices/generic-service.service';


@Component({
    selector: 'generic-detail',
    template: `
		<div class="container-fluid">
		  <div class="row">
		    <div class="col-md-12">
		      <!--Body content-->
		      <h2>Generic Detail</h2>
		      <h4>{{item.name}}</h4>
		      <p>{{item.description}}</p>
		      <a href="/list">Back</a>
		    </div>
		  </div>
		</div>
    `
})

export class GenericDetailComponent implements OnInit{
	private title:string = "Generic Detail";
	private item:object = {}
	private id:any;

	constructor(
		private genericService: GenericService,
		private route: ActivatedRoute,
	) { }

	ngOnInit():void {
		this.route.params.subscribe(id => this.id = id.id);
		this.item = this.genericService.getById(this.id);
	}
}