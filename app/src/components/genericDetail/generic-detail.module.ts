import { NgModule,  }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {GenericDetailComponent } from "./generic-detail.component";

@NgModule({
	imports: [ 
		BrowserModule
	],
	declarations: [ 
		GenericDetailComponent
	],
	exports: [ 
		GenericDetailComponent
	]
})
export class GenericDetailModule { }