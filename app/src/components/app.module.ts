import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import{ AppComponent } from './app.component';
import {AppRoutingModule } from "./app-routing.module";
import {GenericListModule } from "./genericList/generic-list.module";
import {GenericDetailModule} from "./genericDetail/generic-detail.module";
import {SharedServicesModule} from "./sharedServices/generic-service.module";

@NgModule({
	imports: [ 
		BrowserModule, 
		SharedServicesModule,
		GenericListModule,
		GenericDetailModule,
		AppRoutingModule
	],
	declarations: [ 
		AppComponent,
	],
	bootstrap: [ AppComponent ]
})
export class AppModule { }