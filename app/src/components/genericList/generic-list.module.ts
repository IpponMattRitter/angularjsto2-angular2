import { NgModule,  }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {GenericListComponent } from "./generic-list.component";

@NgModule({
	imports: [ 
		BrowserModule
	],
	declarations: [ 
		GenericListComponent,
	],
	exports: [ 
		GenericListComponent
	]
})
export class GenericListModule { }