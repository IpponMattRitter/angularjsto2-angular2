import {Component, OnInit} from '@angular/core';
import {GenericService} from '../sharedServices/generic-service.service';
@Component({
    selector: 'generic-list',
    template: `
		<div class="container-fluid">
		  <div class="row">
		    <div class="col-md-12">
		      <!--Body content-->
		      <h2>Generic List</h2>
		      <ul>
		        <li *ngFor="let item of listItems">
		          <a href="/item/{{item.id}}" >
		            <h4>{{item.name}}</h4>
		            <p>{{item.description}}</p>
		          </a>
		        </li>
		      </ul>
		    </div>
		  </div>
		</div>
    `
})

export class GenericListComponent implements OnInit{
	private listItems:Array<Object>;

	constructor(private genericService: GenericService) { }

	ngOnInit():void {
		this.listItems = this.genericService.getAll();
	}
}