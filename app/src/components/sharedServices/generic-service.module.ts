import { NgModule,  }      from '@angular/core';
import {GenericService } from "./generic-service.service";

@NgModule({
	providers: [
		GenericService
	]
})
export class SharedServicesModule { }